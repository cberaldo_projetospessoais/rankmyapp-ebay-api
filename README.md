# RankMyApp - Teste

## Sumário

- [Tecnologias utilizadas](#tecnologias-utilizadas)
- [Executando a aplicação em modo de produção](#executando-a-aplicação-em-modo-de-produção)
- [Executando em modo de desenvolvimento](#executando-em-modo-de-desenvolvimento)
- [Requisições via cURL](#requisições-via-cURL)

## Tecnologias utilizadas

Como framework de desenvolvimento das `apis` foi utilizado o [NestJS](https://docs.nestjs.com/), com persistência não relacional com [MongoDB](https://www.mongodb.com/).

Para o desenvolvimento da `UI` foi utilizado [ReactJS](https://pt-br.reactjs.org/) juntamente ao [Redux](https://redux.js.org/) e o middleware [ReduxThunk](https://github.com/reduxjs/redux-thunk) para melhor desempenho da UI e qualidade de código.

Foi criada uma estrutura de `conteiner` da aplicação utilizando [Docker](https://www.docker.com/) para facilitar a implantação e execução da aplicação.

Todas as aplicações foram desenvolvidas utilizando o [AirBNB styleguide](https://github.com/airbnb/javascript) e [TypeScript](https://www.typescriptlang.org/).

## Executando a aplicação em modo de produção

```bash
# Instalar dependências do projeto.
$ npm install

# Criar container, realizar build do projeto e inciar.
$ docker-compose up --build
```

## Executando em modo de desenvolvimento

1. API (rankmyapi)

Na pasta do projeto `./rankmyapi`:

```bash
# Instalar dependências do projeto.
$ npm install

# Iniciar api em modo de desenvolvimento
$ npm run start:dev
```

Caso esteja executando em um ambiente `windows` utilizar:

```bash
# Instalar dependências do projeto.
$ npm install

# Iniciar api em modo de desenvolvimento
$ npm run win:dev
```

2. UI (rankmyapp)

Na pasta do projeto `./rankmyapp`:

```bash
# Instalar dependências do projeto.
$ npm install

# Iniciar UI em modo de desenvolvimento.
$ npm run dev

# Realizar build do projeto para produção.
$ npm run build
```

## Requisições via UI

A UI do aplicativo fica disponível em [http://localhost:3000](http://localhost:3000) quando [executando a aplicação em modo de produção](#executando-a-aplicação-em-modo-de-produção).

## Requisições via cURL

Para o campo `alertEvery` deve ser informado um dos seguintes valores:

- "MINUTES_02" (a cada 2 minutos)
- "MINUTES_10" (a cada 10 minutos)
- "MINUTES_30" (a cada 30 minutos)

1. Criar um alerta de busca

```curl
curl -X POST \
  http://localhost:3000/search-alert \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "email": "cberaldo.desenvolvimento@outlook.com",
    "searchPhrase": "boss katana 100",
    "alertEvery": "MINUTES_02"
  }'
```

2. Remover um alerta de busca

```curl
curl -X DELETE \
  http://localhost:3000/search-alert \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "_id": "5d2e27f15449b231ccaa62e0"
  }'
```

3. Alterar um alerta de busca

```curl
curl -X PUT \
  http://localhost:3000/search-alert \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "_id": "5d2f7660ede1fc32cc7a6b1a",
    "email": "caio.beraldo@fatec.sp.gov.br",
    "searchPhrase": "seize mark oz II",
    "alertEvery": "MINUTES_02"
  }'
```

4. Buscar um alerta

```curl
curl -X GET \
  http://localhost:3000/search-alert/5d2f7660ede1fc32cc7a6b1a \
  -H 'Accept: */*' \
  -H 'cache-control: no-cache'
```

5. Listar todos os alertas

```curl
curl -X GET \
  http://localhost:3000/search-alert \
  -H 'Accept: */*' \
  -H 'cache-control: no-cache'
```

