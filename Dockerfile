FROM node:11

WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install app dependencies
# If you are building your code for production
# RUN npm ci --only=production
RUN npm install

# Bundle app source
COPY . .
# COPY .env* ./dist

COPY rankmyapi/.env*.* dist/

EXPOSE 3000
# EXPOSE 1234

CMD ["npm", "run", "start"]
