import { IsEmail, IsNotEmpty, IsEnum, MinLength, MaxLength, IsMongoId } from 'class-validator'
import { MINUTES } from '../search-alert.schema'

export class UpdateSearchAlertPayload {
  @IsMongoId()
  // tslint:disable-next-line:variable-name
  _id: number
  @IsEmail()
  @IsNotEmpty()
  email: string
  @MinLength(5)
  @MaxLength(30)
  @IsNotEmpty()
  searchPhrase: string
  @IsEnum(MINUTES)
  @IsNotEmpty()
  alertEvery: number
}
