import { IsMongoId } from 'class-validator'

export class GetSearchAlertPayload {
  @IsMongoId()
  // tslint:disable-next-line:variable-name
  _id: number
}
