import { IsEmail, IsNotEmpty, IsEnum, MinLength, MaxLength } from 'class-validator'
import { MINUTES } from '../search-alert.schema'

export class CreateSearchAlertPayload {
  @IsEmail()
  @IsNotEmpty()
  email: string
  @MinLength(5)
  @MaxLength(30)
  @IsNotEmpty()
  searchPhrase: string
  @IsEnum(MINUTES)
  @IsNotEmpty()
  alertEvery: number
}
