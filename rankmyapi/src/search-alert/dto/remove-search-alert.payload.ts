import { IsMongoId } from 'class-validator'

export class RemoveSearchAlertPayload {
  @IsMongoId()
  // tslint:disable-next-line:variable-name
  _id: number
}
