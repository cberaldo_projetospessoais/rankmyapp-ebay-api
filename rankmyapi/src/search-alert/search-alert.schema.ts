import { Schema } from 'mongoose'

const timestamps = { createdAt: 'createdAt', updatedAt: 'updatedAt' }

export enum MINUTES {
  MINUTES_02 = 120000,
  MINUTES_10 = 600000,
  MINUTES_30 = 1800000,
}

export const SearchAlertSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  searchPhrase: {
    type: String,
    required: true,
  },
  alertEvery: {
    type: MINUTES,
    required: true,
  },
}, {
  timestamps,
})

// Chave única - não pode existir mais de um alerta igual para o mesmo e-mail.
SearchAlertSchema.index({ email: 1, searchPhrase: -1 }, { unique: true })

export interface SearchAlert extends Document {
  readonly id: string
  readonly email: string
  readonly searchPhrase: string
  readonly alertEvery: number
  readonly createdAt: Date
  readonly updatedAt: Date
}
