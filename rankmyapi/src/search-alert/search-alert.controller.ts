import { Controller, Get, Post, Body, Delete, HttpCode, Put, Param } from '@nestjs/common'
import { SearchAlertService } from './search-alert.service'
import { CreateSearchAlertPayload } from './dto/create-search-alert.payload'
import { RemoveSearchAlertPayload } from './dto/remove-search-alert.payload'
import { GetSearchAlertPayload } from './dto/get-search-alert.payload'
import { UpdateSearchAlertPayload } from './dto/update.search-alert.payload'

@Controller('search-alert')
export class SearchAlertController {

  constructor(private readonly searchAlertService: SearchAlertService) {}

  private handleMongoError(error) {
    if (error.name !== 'MongoError') {
      return
    }

    const mapError = (mongoError) => ({
      error: mongoError.name,
      message: [{ constraints: { mongoError: ['duplicate key for provided request'] } }],
      statusCode: 400,
    })

    switch (error.code) {
      case 11000:
        return mapError(error)
      default:
        return
    }
  }

  @Get('/')
  async getAlerts() {
    try {
      return await this.searchAlertService.getAlerts()
    } catch (error) {
      return error
    }
  }

  @Post('/')
  @HttpCode(200)
  async createSearchAlert(@Body() payload: CreateSearchAlertPayload) {
    try {
      return await this.searchAlertService.createAlert(payload)
    } catch (error) {
      return this.handleMongoError(error)
    }
  }

  @Delete('/')
  async removeSearchAlert(@Body() payload: RemoveSearchAlertPayload) {
    try {
      return await this.searchAlertService.removeAlert(payload)
    } catch (error) {
      return error
    }
  }

  @Get('/:_id')
  async getAlert(@Param() payload: GetSearchAlertPayload) {
    try {
      return await this.searchAlertService.getAlert(payload)
    } catch (error) {
      return error
    }
  }

  @Put('/')
  async updateAlert(@Body() payload: UpdateSearchAlertPayload) {
    try {
      return await this.searchAlertService.updateAlert(payload)
    } catch (error) {
      return error
    }
  }

}
