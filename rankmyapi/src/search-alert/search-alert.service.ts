import { Injectable, Inject } from '@nestjs/common'
import { Model } from 'mongoose'
import { CreateSearchAlertPayload } from './dto/create-search-alert.payload'
import { RemoveSearchAlertPayload } from './dto/remove-search-alert.payload'
import { UpdateSearchAlertPayload } from './dto/update.search-alert.payload'
import { GetSearchAlertPayload } from './dto/get-search-alert.payload'
import { SearchAlert } from './search-alert.schema'
import { EmailService } from '../email/email.service'
import { CONSTANTS } from '../constants'

@Injectable()
export class SearchAlertService {

  constructor(
    @Inject(CONSTANTS.SEARCH_ALERT_MODEL)
    private readonly searchAlert: Model<SearchAlert>,
    private readonly emailService: EmailService,
   ) {}

  async getAlerts(filter?): Promise<SearchAlert[]> {
    return await this.searchAlert.find(filter).exec()
  }

  async createAlert(payload: CreateSearchAlertPayload): Promise<SearchAlert> {
    const alert = new this.searchAlert(payload).save()

    // enviar e-mail com a link para cancelar alerta.
    this.emailService.sendSearchAlertCreatedEmail({
      to: payload.email,
      searchPhrase: payload.searchPhrase,
    })

    return alert
  }

  async updateAlert(payload: UpdateSearchAlertPayload): Promise<SearchAlert> {
    return this.searchAlert.updateOne({
        _id: payload._id
      }, { 
      $set: {
        email: payload.email,
        searchPhrase: payload.searchPhrase,
        alertEvery: payload.alertEvery,
      } 
    })
  }

  async removeAlert(payload: RemoveSearchAlertPayload): Promise<SearchAlert> {
    const alert = await this.searchAlert.findOne({ ...payload }).exec()
    if (!alert) {
      return new this.searchAlert()
    }
    return await alert.remove()
  }

  async getAlert(payload: GetSearchAlertPayload): Promise<SearchAlert> {
    return this.searchAlert.findOne({ _id: payload._id }).lean()
  }

}
