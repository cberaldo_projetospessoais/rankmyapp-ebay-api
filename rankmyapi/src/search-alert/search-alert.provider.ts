import { Connection } from 'mongoose'
import { SearchAlertSchema } from './search-alert.schema'

export const searchAlertProvider = [
  {
    provide: 'SEARCH_ALERT_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('SearchAlert', SearchAlertSchema),
    inject: ['DATABASE_CONNECTION'],
  },
]
