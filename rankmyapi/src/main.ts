import { NestFactory } from '@nestjs/core'
import { Logger, ValidationPipe } from '@nestjs/common'
import { NestExpressApplication } from '@nestjs/platform-express'
import { json, urlencoded } from 'body-parser'
import { CONSTANTS } from './constants'
import * as dotenv from 'dotenv'

// carregar arquivos de configuração de ambiente.
// precisa estar antes da importação do módulo principal da aplicação por que ele
// vai iniciar todos os processos inclusive o de conexão com o banco.
const env = !process.env.NODE_ENV ? CONSTANTS.NODE_ENV_DEVELOPMENT : process.env.NODE_ENV
const path = (env === CONSTANTS.NODE_ENV_PRODUCTION) ? `/usr/src/app/dist/` : ''
const result = dotenv.config({ path: `${path}.env.${env}` })

if (result.error) {
  throw result.error
}

Logger.warn(`Using ${env} environment`, 'RankMyApp')

import { AppModule } from './app.module'
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule)
  const port = process.env.PORT

  // Configurações da API.
  app.use(json()) // habilitar recebimento de dados em formato json
  app.use(urlencoded({ extended: true })) // habilitar recebimento de dados em formato urlencoded
  app.enableCors() // habilitar requisições cross-origin

  // Configurações do servidor (production only).
  app.useStaticAssets(join(__dirname, 'public'))

  // Validação das informações enviadas nas requisições.
  app.useGlobalPipes(new ValidationPipe())

  await app.listen(port, () => {
    Logger.log(`App listening on http://localhost:${port}`, CONSTANTS.APP_NAME)
  })
}
bootstrap()
