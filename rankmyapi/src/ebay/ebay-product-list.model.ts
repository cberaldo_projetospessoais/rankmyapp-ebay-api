import EBayProduct from './ebay-product.model'

export default class EBayProductList extends Array<EBayProduct> {
  toHTML(searchPhrase: string): string {
    const html = []

    if (this.length === 0) {
      html.push(`<h1>Não encontramos produtos para a busca "${searchPhrase}"</h1>`)
      html.push(`<h3>© Copyright 2019. Todos os direitos reservados.</h3>`)
      return html.join('')
    }

    html.push(`<h1>Melhores produtos encontrados para "${searchPhrase}"</h1>`)
    html.push(`<div style="display: flex !important; flex-wrap: wrap !important; padding: 5px !important;">`)
    this.forEach((product) => {
      html.push(`<a target="_blank" href="${product.viewItemURL}">
        <div style="display: flex !important; flex-direction: column !important; width: 160px !important; padding: 5px !important;">
          <img src="${product.galleryURL}" style="width: 160px !important; height: 120px !important; border-radius: 5px !important;">
          <span>${product.title} (<b>${product.currencyId} ${product.currentPrice.toFixed(2)}</b>)</span>
          <span>${product.location}</span>
          ${product.timeLeft}
        </div>
      </a>`)
    })
    html.push(`</div>`)
    html.push(`<h3>© Copyright 2019. Todos os direitos reservados.</h3>`)

    return html.join('')
  }
}
