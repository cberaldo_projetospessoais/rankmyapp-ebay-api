import { Injectable } from '@nestjs/common'
import { RESTClient } from './api/api.base'
import EBayProductList from './ebay-product-list.model'
import { CONSTANTS } from 'dist/constants';

@Injectable()
export class EbayApiClient {
  private client: RESTClient

  constructor() {
    this.client = new RESTClient(process.env.EBAY_API_URL)
  }

  private mapAndSortJsonToEBayProductList(data): EBayProductList {
    const eBayProductList = new EBayProductList()

    if (!data.findItemsByKeywordsResponse) {
      return eBayProductList
    }

    const productList: any[] = data.findItemsByKeywordsResponse[0].searchResult[0].item

    productList.forEach((item) => eBayProductList.push({
      title: item.title[0],
      galleryURL: item.galleryURL[0],
      viewItemURL: item.viewItemURL[0],
      location: item.location[0],
      condition: item.condition[0].conditionDisplayName[0],
      currencyId: item.sellingStatus[0].currentPrice[0]['@currencyId'],
      currentPrice: Number(item.sellingStatus[0].currentPrice[0].__value__),
      timeLeft: item.sellingStatus[0].timeLeft[0],
      sellingState: item.sellingStatus[0].sellingState[0],
    }))

    return eBayProductList.sort((a, b) => a.currentPrice < b.currentPrice ? -1 : 1)
  }

  fetchTop3ProductsByLowestPrice(searchPhrase: string): Promise<EBayProductList> {
    const ea = this
    return new Promise(async (resolve) => {
      let sandbox = { name: 'env', value: process.env.EBAY_API_ENV }

      ea.client.addParams([
        sandbox,
        { name: 'OPERATION-NAME', value: 'findItemsByKeywords' },
        { name: 'SERVICE-VERSION', value: '1.0.0' },
        { name: 'SECURITY-APPNAME', value: process.env.EBAY_APP_ID },
        { name: 'RESPONSE-DATA-FORMAT', value: 'JSON' },
        { name: 'REST-PAYLOAD', value: '' },
        { name: 'keywords', value: searchPhrase },
        { name: 'paginationInput.pageNumber', value: '1' },
        { name: 'paginationInput.entriesPerPage', value: '3' },
      ])

      const data = await this.client.get()
      const eBayProductList = ea.mapAndSortJsonToEBayProductList(data)
      resolve(eBayProductList)
    })
  }

}
