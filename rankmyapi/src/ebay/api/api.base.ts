import { Injectable } from '@nestjs/common'
import { ClientRequest, RequestOptions } from 'http'
import * as http from 'https'
import Param from './param.model'

@Injectable()
export class RESTClient {
  endpoint: string
  searchParams: URLSearchParams
  options: RequestOptions = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive',
      'cache-control': 'no-cache',
    },
  }
  request: ClientRequest

  constructor(endpoint: string) {
    this.endpoint = endpoint
    this.searchParams = new URLSearchParams()
  }

  private makeRequest(options: RequestOptions): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = `${this.endpoint}?${this.searchParams.toString()}`

      const req = http.request(url, options, (res) => {
        const chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => resolve(JSON.parse(Buffer.concat(chunks).toString())))
        res.on('error', (error) => reject(error))
      })

      req.end()
    })
  }

  addParams(params: Param[]) {
    const ab = this
    ab.searchParams = new URLSearchParams()
    params.forEach(param => ab.searchParams.append(param.name, param.value))
  }

  get(): Promise<any> {
    return this.makeRequest({ ...this.options })
  }

}
