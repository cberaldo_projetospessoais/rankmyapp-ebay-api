
export default class EBayProduct {
  title: string
  galleryURL: string
  viewItemURL: string
  location: string
  condition: object[]
  currencyId: string
  currentPrice: number
  timeLeft: string
  sellingState: string
}
