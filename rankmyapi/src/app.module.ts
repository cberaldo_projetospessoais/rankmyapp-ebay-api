import { Module } from '@nestjs/common'
import { EmailWorkService } from './email/email.workservice'
import { EmailService } from './email/email.service'
import { SearchAlertController } from './search-alert/search-alert.controller'
import { SearchAlertService } from './search-alert/search-alert.service'
import { DatabaseModule } from './database/database.module'
import { EbayApiClient } from './ebay/ebay.api'

@Module({
  imports: [DatabaseModule],
  controllers: [SearchAlertController],
  providers: [SearchAlertService, EmailService, EmailWorkService, EbayApiClient],
})
export class AppModule {
  constructor(private readonly emailWorkService: EmailWorkService) {
    this.emailWorkService.startSearchAlertService()
  }
}
