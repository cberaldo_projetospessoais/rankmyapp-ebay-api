import { Injectable, Logger } from '@nestjs/common'
import { SearchAlertEmailPayload } from './dto/search-alert-email.payload'
import { SearchAlertCreatedEmailPayload } from './dto/search-alert-created.payload'
import * as mailer from 'nodemailer'
import { CONSTANTS } from '../constants'

@Injectable()
export class EmailService {
  private transporter
  private from

  async initialize() {
    // create reusable transporter object using the default SMTP transport
    let transporterOptions
    if (process.env.NODE_ENV === CONSTANTS.NODE_ENV_PRODUCTION) {
      this.from = `"Alerta de Produtos e-Bay" <${process.env.EMAIL_USER}>`

      transporterOptions = {
        service: process.env.EMAIL_SERVICE,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASS,
        },
      }
    } else {
      const testAccount = await mailer.createTestAccount()

      this.from = `"Alerta de Produtos e-Bay" <${testAccount.user}>`

      transporterOptions = {
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: testAccount.user, // generated ethereal user
          pass: testAccount.pass, // generated ethereal password
        },
      }
    }

    this.transporter = mailer.createTransport(transporterOptions)
  }

  async sendSearchAlertEmail(payload: SearchAlertEmailPayload): Promise<any> {
    const subject = payload.products.length === 0 ?
    `Nenhum produto encontrado para "${payload.searchPhrase}"` :
    `Encontramos algums produtos para "${payload.searchPhrase}" que podem te interessar`

    const info = await this.transporter.sendMail({
      from: this.from,
      to: payload.to,
      subject,
      html: payload.products.toHTML(payload.searchPhrase),
    })

    if (process.env.NODE_ENV === CONSTANTS.NODE_ENV_DEVELOPMENT) {
      Logger.log(info.messageId, 'Message sent')
      Logger.log(mailer.getTestMessageUrl(info), 'Preview URL')
    }
    return info
  }

  async sendSearchAlertCreatedEmail(payload: SearchAlertCreatedEmailPayload): Promise<any> {
    const info = await this.transporter.sendMail({
      from: this.from,
      subject: `Alerta de Produtos e-Bay`,
      to: payload.to,
      html: `<h1>Alerta de produtos e-Bay</h1>
        <p>Novo alerta para produtos foi criado para a busca "${payload.searchPhrase}".</p>
        <p>Logo enviaremos um e-mail com os melhores produtos encontrados.</p>
        <h3>© Copyright 2019. Todos os direitos reservados.</h3>
      `})

      if (process.env.NODE_ENV === CONSTANTS.NODE_ENV_DEVELOPMENT) {
        Logger.log(info.messageId, 'Message sent')
        Logger.log(mailer.getTestMessageUrl(info), 'Preview URL')
      }
      return info
  }
}
