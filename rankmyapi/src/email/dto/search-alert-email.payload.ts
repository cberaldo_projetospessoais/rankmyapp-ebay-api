import EBayProductList from '../../ebay/ebay-product-list.model'

export class SearchAlertEmailPayload {
  to: string
  searchPhrase: string
  products: EBayProductList
}
