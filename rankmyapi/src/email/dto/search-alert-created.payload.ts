
export class SearchAlertCreatedEmailPayload {
  to: string
  searchPhrase: string
}