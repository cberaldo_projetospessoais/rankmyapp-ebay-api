import { Logger, Injectable } from '@nestjs/common'
import { EmailService } from './email.service'
import { SearchAlertService } from '../search-alert/search-alert.service'
import { EbayApiClient } from '../ebay/ebay.api'
import { SearchAlert, MINUTES } from '../search-alert/search-alert.schema'
import { SearchAlertEmailPayload } from './dto/search-alert-email.payload'
import EBayProductList from '../ebay/ebay-product-list.model'
import { CONSTANTS } from '../constants'

@Injectable()
export class EmailWorkService {
  workers: NodeJS.Timeout[] = []

  constructor(
    private readonly emailService: EmailService,
    private readonly searcAlertService: SearchAlertService,
    private readonly ebayApiClient: EbayApiClient,
  ) {
    this.emailService.initialize()
  }

  private async fetchProductsAndSendEmail(alert: SearchAlert) {
    const products: EBayProductList = await this.ebayApiClient
      .fetchTop3ProductsByLowestPrice(alert.searchPhrase)

    const payload: SearchAlertEmailPayload = {
      to: alert.email,
      searchPhrase: alert.searchPhrase,
      products,
    }
    return this.emailService.sendSearchAlertEmail(payload)
  }

  private sendSearchAlertsFor(timeout: string) {
    const ew = this
    return async () => {
      const alerts = await ew.searcAlertService.getAlerts({ alertEvery: timeout })
      alerts.forEach(alert => ew.fetchProductsAndSendEmail(alert))
    }
  }

  private configureWorker(timeout: string) {
    const worker = setInterval(this.sendSearchAlertsFor(timeout), MINUTES[timeout])
    this.workers.push(worker)
  }

  startSearchAlertService = () => {
    const ew = this

    // iterar todos os tempos de envio configurados para configurar um worker para cada.
    for (const timeout in MINUTES) {
      // ignorar valores do enum.
      if (!isNaN(Number(timeout))) {
        continue
      }
      ew.configureWorker(timeout)
    }

    Logger.warn('Serviço de envio de e-mails iniciado.', CONSTANTS.APP_NAME)
  }

  stopSearchAlertService = () => {
    const ew = this
    ew.workers.forEach(worker => clearInterval(worker))
    ew.workers = []

    Logger.warn('Serviço de envio de e-mails encerrado.', CONSTANTS.APP_NAME)
  }

}
