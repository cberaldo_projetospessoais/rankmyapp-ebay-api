import { Module } from '@nestjs/common'
import { databaseProviders } from './database.providers'
import { searchAlertProvider } from '../search-alert/search-alert.provider'

const providers = [
  ...databaseProviders,
  ...searchAlertProvider,
]

@Module({
  providers,
  exports: providers,
})
export class DatabaseModule {}
