import * as mongoose from 'mongoose'
import { CONSTANTS } from '../constants'

export const databaseProviders = [
  {
    provide: CONSTANTS.DATABASE_CONNECTION,
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }),
  },
]
