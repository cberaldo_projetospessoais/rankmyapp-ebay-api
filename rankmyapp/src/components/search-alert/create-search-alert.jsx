import React from 'react'
import { formToJSON } from '../utils'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchCreateSearchAlert } from '../../store/actions/search-alert.api'
import { createAlert, cancelCreateAlert } from '../../store/actions'

const CreateSearchAlert = ({ isCreatingAlert, createAlert, cancelCreateAlert, fetchCreateSearchAlert }) => {
  if (!isCreatingAlert) {
    return <button className="button primary" onClick={() => createAlert()}>Adicionar Alerta</button>
  }

  return (
    <div className="container">
      <h2>Criar novo alerta</h2>

      <form id="create-search-alert" className="flex-box">
        <input 
          type="email" 
          name="email" 
          className="input" 
          placeholder="Enviar para o e-mail..." />

        <input 
          type="text" 
          name="searchPhrase" 
          className="input"
          placeholder="Buscar pelos produtos..." />

        <select name="alertEvery" className="input">
          <option value="MINUTES_02">2 minutos</option>
          <option value="MINUTES_10">10 minutos</option>
          <option value="MINUTES_30">30 minutos</option>
        </select>

        <button
          type="button" 
          className="button primary"
          onClick={() => {
            const alertForm = document.getElementById('create-search-alert')
            const alert = formToJSON(alertForm)

            fetchCreateSearchAlert(alert)
              .then(() => alertForm.reset())
          }}>
          Criar Alerta
        </button>
      </form>

      <button className="button danger" onClick={() => cancelCreateAlert()}>Cancelar</button>
    </div>
  )
}

const mapStateToProps = state => ({
  isCreatingAlert: state.isCreatingAlert
 })

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCreateSearchAlert,
  createAlert,
  cancelCreateAlert
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CreateSearchAlert)
