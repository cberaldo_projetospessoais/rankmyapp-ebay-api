import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchListSearchAlerts, fetchRemoveSearchAlert } from '../../store/actions/search-alert.api'

const ListSearchAlert = ({ searchAlerts, fetchListSearchAlerts, fetchRemoveSearchAlert }) => {
  useEffect(() => { fetchListSearchAlerts() }, [])

  return (
    <div>
      <h2>Meus Alertas
        <button 
          className="button primary"
          onClick={fetchListSearchAlerts}>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
          </button>
      </h2>

      <div className="container">
        {searchAlerts
          .sort((a, b) =>
            new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1)
          .map(searchAlert =>
            <SearchAlertItem key={searchAlert._id} searchAlert={searchAlert} fetchRemoveSearchAlert={fetchRemoveSearchAlert} />
        )}
      </div>
    </div>
  )
}

const SearchAlertItem = ({ searchAlert, fetchRemoveSearchAlert }) => {
  const createdAt = new Date(searchAlert.createdAt)
  let timeSinceCreated = Number(((new Date() - createdAt) / 1000 / 60).toFixed())

  return (
    <div className="flex-box card">
      {<SearchAlertTimeBadge alertEvery={searchAlert.alertEvery} />}

      <h3 className="no-wrap-text">{searchAlert.searchPhrase}</h3>
      <span className="no-wrap-text">{searchAlert.email}</span>
      <span>criado {timeSinceCreated <= 60 ? 
        `há ${timeSinceCreated} minutos.` : 
        'há mais de uma hora.'}
      </span>

      <button 
        className="button danger"
        type="button"
        onClick={() => fetchRemoveSearchAlert(searchAlert._id)}>
        Remover Alerta
      </button>
    </div>
  )
}

const SearchAlertTimeBadge = ({ alertEvery }) => {
  switch (alertEvery) {
    case 'MINUTES_02':
      return (<span className="badge warning">2 min</span>)
    case 'MINUTES_10':
      return (<span className="badge primary">10 min</span>)
    case 'MINUTES_30':
      return (<span className="badge default">30 min</span>)
    default:
      return null
  }
}

const mapStateToProps = (state) => ({ searchAlerts: state.searchAlerts })

const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchListSearchAlerts, fetchRemoveSearchAlert }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ListSearchAlert)
