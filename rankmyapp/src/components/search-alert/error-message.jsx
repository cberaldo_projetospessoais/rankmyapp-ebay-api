import React from 'react'
import { connect } from 'react-redux'

const ErrorMessage = ({ error_message }) => {
  const handleErros = (errors) => (
    <>
      {errors.map(error =>
        error.errors.map((error, index) => (
          <p key={index}>{error}</p>
        ))
      )}
    </>
  )

  return (
    <div className="error-message"> {
      Array.isArray(error_message) ? 
        handleErros(error_message) :
        error_message
    }</div>
  )
}

const mapStateToProps = (state) => ({ error_message: state.error_message })

export default connect(mapStateToProps)(ErrorMessage)