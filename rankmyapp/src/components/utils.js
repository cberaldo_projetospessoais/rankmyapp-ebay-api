
export const formToJSON = elements => [].reduce.call(elements, (data, element) => {
  if (element.type === 'button') {
    return data
  }

  data[element.name] = element.value
  return data
}, {})
