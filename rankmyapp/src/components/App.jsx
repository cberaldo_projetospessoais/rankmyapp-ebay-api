import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import CreateSearchAlert from './search-alert/create-search-alert'
import ListSearchAlert from './search-alert/list-search-alerts'
import ErrorMessage from './search-alert/error-message'

import '../search-alert.css'
import '../index.css'

const App = ({ }) => {
  return (
    <div>
      <h1>Alertas de Produtos e-Bay</h1>

      <CreateSearchAlert />

      <ErrorMessage />

      <ListSearchAlert />
    </div>
  )
}

const mapStateToProps = state => ({ })
const mapDispatchToProps = dispatch => bindActionCreators({ }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
