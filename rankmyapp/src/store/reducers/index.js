import { SET_CREATING_ALERT, SEARCH_ALERT_API } from '../actions/types'

const DEFAULT_SETTINGS = {
  isCreatingAlert: false,
  searchAlerts: [],
}

const rootReducer = (state = DEFAULT_SETTINGS, action) => {
  switch (action.type) {
    case SET_CREATING_ALERT:
      return { ...state, isCreatingAlert: action.isCreatingAlert }
    case SEARCH_ALERT_API.FETCH_SUCCESS:
      return { ...state, searchAlerts: action.searchAlerts }
    case SEARCH_ALERT_API.FETCH_ERROR:
      return { ...state, error_message: action.message }
    case SEARCH_ALERT_API.FETCH_CREATE_SUCCESS:
      return { ...state, searchAlerts: [...state.searchAlerts, action.alert] }
    case SEARCH_ALERT_API.FETCH_CREATE_ERROR:
      return { ...state, error_message: action.message }
    case SEARCH_ALERT_API.FETCH_REMOVE_SUCCESS:
      return { ...state, searchAlerts: state.searchAlerts.filter(sa => sa._id !== action.alert._id) }
    case SEARCH_ALERT_API.FETCH_REMOVE_ERROR:
      return { ...state, error_message: action.message }
    default:
      return state
  }
}

export default rootReducer
