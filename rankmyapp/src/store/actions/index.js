import { SET_CREATING_ALERT } from './types'

export const createAlert = () => ({
  type: SET_CREATING_ALERT,
  isCreatingAlert: true,
})

export const cancelCreateAlert = () => ({
  type: SET_CREATING_ALERT,
  isCreatingAlert: false,
})
