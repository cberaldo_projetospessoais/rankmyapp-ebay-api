import { SEARCH_ALERT_API } from './types'

export const fetchSearchAlertSuccess = (alerts) => ({
  type: SEARCH_ALERT_API.FETCH_SUCCESS,
  searchAlerts: alerts
})

export const fetchSearchAlertError = ({ message }) => ({
  type: SEARCH_ALERT_API.FETCH_ERROR,
  message
})

export const fetchCreateSearchAlertSuccess = (alert) => ({
  type: SEARCH_ALERT_API.FETCH_CREATE_SUCCESS,
  alert
})
  
export const fetchCreateSearchAlertError = ({ message }) => ({
  type: SEARCH_ALERT_API.FETCH_CREATE_ERROR,
  message
})

export const fetchRemoveSearchAlertSuccess = (alert) => ({
  type: SEARCH_ALERT_API.FETCH_REMOVE_SUCCESS,
  alert
})

export const fetchRemoveSearchAlertError = ({ message }) => ({
  type: SEARCH_ALERT_API.FETCH_REMOVE_ERROR,
  message
})

const handlerErrors = (data) => {
  if (data.error) {
    const erros = data.message.map(message => {
      const errors = Object.keys(message.constraints)
        .map(key => message.constraints[key])

      return {
        property: message.property,
        errors
      }
    })

    return Promise.reject({ message: erros})
  }

  return Promise.resolve(data)
}

const handleResponse = (response) => {
  // if (response.status === 200) {
  //  throw new Error('Unsuccessiful request to localhost:3000')
  // }
  return response.json()
}

const options = {
  "method": "GET",
  "crossDomain": true,
  "headers": {
    "Content-Type": "application/json",
    "Accept": "*/*",
  }
}

const searchAlertApiUrl = 'http://localhost:3000/search-alert'

export const fetchListSearchAlerts = () => (dispatch) => {
  return fetch(searchAlertApiUrl, options)
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(alerts => dispatch(fetchSearchAlertSuccess(alerts)))
    .catch(error => dispatch(fetchSearchAlertError(error)))
}

export const fetchCreateSearchAlert = (searchAlert) => (dispatch) => {
  return fetch(searchAlertApiUrl, {
      ...options,
      method: 'POST',
      body: JSON.stringify(searchAlert)
    })
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(alert => dispatch(fetchCreateSearchAlertSuccess(alert)))
    .then(() => fetchListSearchAlerts())
    .catch(error => dispatch(fetchCreateSearchAlertError(error)))
}

export const fetchRemoveSearchAlert = (_id) => (dispatch) => {
  return fetch(searchAlertApiUrl, {
      ...options,
      method: 'DELETE',
      body: JSON.stringify({ _id })
    })
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(alert => dispatch(fetchRemoveSearchAlertSuccess(alert)))
    .catch(error => dispatch(fetchRemoveSearchAlertError(error)))
}
    
